/*
    Kurtis's HiFi Online friends list.

    

*/
const electron = require('electron');
const {app, Tray, Menu, BrowserWindow, ipcMain} = require('electron');
const path = require('path');
const storage = require('electron-json-storage');
const request = require('request');
const url = require('url');
const iconPath = path.join(__dirname, '/app/img/people-i.png');
let appIcon = null;
let win = null;

var onlineList = [];
var isDebug = true;
var isQuit = false;
var settings;
var onlineTimer;

function windowToggle() {
    if (win.isVisible()) {
        if (!win.isMaximized()) {
            settings.window = win.getBounds();
        }
        win.hide();
        settings.visible = false;
    } else {
        win.show();
        settings.visible = true;
    }
    save();
}

function initList() {
    clearInterval(onlineTimer);
    win.webContents.send('app-click', {cmd:"clearList"});
    onlineList = [];
    if (settings.userApiKey !== '') {
        getUserList(1,'');
        onlineTimer = setInterval(function () {
            getUserList(1,'online');
        }, 60000);
    } else {
        win.webContents.send('app-click', {cmd:"ui-msg", message:"No API key found, go into the settings to find out more."});
    }
}

function save() {
    storage.set('settings', settings, function(error) {
        if (error) throw error;
    });
}

function getUserList(index, status) {
    request({
        uri: settings.metaverseServerUrl+'/api/v1/users?filter=connections&page='+index+'&status='+status+'&per_page=100',
        method: 'GET',
        auth: { 'bearer': settings.userApiKey }
    }, function(error, response, body) {
        if (error) {
            win.webContents.send('app-click', {cmd:"ui-msg", message:"Could not connect to Metaverse API."});
        } else if (response.statusCode === 200) {
            try {
                body = JSON.parse(body);
                if (body.current_page === 1 && body.total_pages === 1 && body.total_entries === 0 && status === '') {
                    // could have no friends or the api is invalid!
                    win.webContents.send('app-click', {cmd:"ui-msg", message:"The API key is invalid! (or you have no connections)"});
                }
                for (var i in onlineList) {
                    var j = 0;
                    for (j = 0; j < body.data.users.length; j++) {
                        if (onlineList[i].username === body.data.users[j].username) {
                            break;
                        }
                    }
                    if (j === body.data.users.length) {
                        onlineList[i].online = false;
                        win.webContents.send('friend-update', onlineList[i]);
                        onlineList.splice(i,1);
                    }
                }
                for (var i in body.data.users) {
                    win.webContents.send('friend-update', body.data.users[i]);
                    // add people online to a temp online list.
                    if (body.data.users[i].online) {
                        var j = 0;
                        for (j = 0; j < onlineList.length; j++) {
                            if (onlineList[j].username === body.data.users[i].username) {
                                break;
                            }
                        }
                        if (j === onlineList.length) {
                            onlineList.push(body.data.users[i]);
                        }
                    }
                }
                if (body.total_pages > body.current_page) {
                    var nextPage = body.current_page + 1;
                    getUserList(nextPage,status);
                }
            } catch (e) {
                win.webContents.send('app-click', {cmd:"ui-msg", message:"Could not connect to Metaverse API. Please check your internet or settings."});
            }
        }
    });
};

app.on('ready',function() {
    // need to turn this into a settings.json grab.
    storage.get('settings', function(error, data) {
        if (JSON.stringify(data) === JSON.stringify({}) || error) {
            // if there is an error we still need to get a dummy config. So run this just means saving wont work.
            const {width, height} = electron.screen.getPrimaryDisplay().workAreaSize;
            settings = {
                version: 1,
                metaverseServerUrl: 'https://metaverse.highfidelity.com',
                userApiKey: '',
                notify:true,
                window: {
                    x: width - 400,
                    y: height - 600,
                    width: 400,
                    height: 600 
                },
                visible: true
            };
            save();
        } else {
            settings = data;
        }
        electron.powerMonitor.on('resume', () => {
            initList();
        });        
        ipcMain.on('settings-update', function(event, arg) {
            for (var i in arg) {
                settings[i] = arg[i];
                switch (i) {
                    case 'metaverseServerUrl':
                        win.webContents.send('app-click', {cmd:"clearList"});
                        initList();
                        break;
                    case 'userApiKey':
                        win.webContents.send('app-click', {cmd:"clearList"});
                        if (settings[i].trim() === '') {
                            settings[i] = null;
                        }
                        initList();
                        break;
                }
            }
            save();
        });
        ipcMain.on('app-click', function(event, arg) {
            switch (arg.cmd) {
                case 'exit': 
                    win.hide();
                    settings.visible = false;
                    save();
                    break;
                case 'minimize':
                    win.minimize();
                    break;
                case 'init': 
                    win.webContents.send('settings-update', settings);
                    initList();
                    break;
                case 'reload': 
                    initList();
                    break;
                case 'minmaxToggle':
                    if (win.isMaximized()) {
                        win.unmaximize();
                    } else {
                        win.maximize();
                    }
                    break;
            }
        });
        win = new BrowserWindow({
                x: settings.window.x,
                y: settings.window.y,
                width: settings.window.width,
                height: settings.window.height,
                show: false,
                icon: iconPath,
                frame: false
        });
        win.loadURL(url.format({
            pathname: path.join(__dirname, '/app/app.html'),
            protocol: 'file:',
            slashes: true
        }));
        win.on('close', function(e) {
            if (isQuit) {
                win = null;
            } else {
                if (!win.isMaximized()) {
                    settings.window = win.getBounds();
                }
                e.preventDefault();
                win.hide();
                settings.visible = false;
                save();
            }
        });
        if (settings.visible) {
            win.show();
        }
        appIcon = new Tray(iconPath);
        var contextMenu = Menu.buildFromTemplate([
            {
                label: 'Open',
                click: function() {
                    windowToggle();
                }
            },
            {
                label: 'Settings',
                submenu: [
                    { label: 'Notifications'},
                    { label: 'Update Interval'}
                ]
            },
            {
                label: 'Exit',
                click: function() {
                    isQuit = true;
                    app.quit();
                }
            }
        ]);
        appIcon.on('click', function(e) {
            windowToggle();
        })
        appIcon.setToolTip("High Fidelity Connections");
        appIcon.setContextMenu(contextMenu);
        // once the settings are loaded kick in the friends list.
    });
});