![image](https://bytebucket.org/thecollectivesl/connections-desktop-app/raw/master/img/connections.png)
# High Fidelity Connections Desktop App #

This is a Desktop application that allows users to view their connections on High Fidelity. You can receive notifications when people come online and also click on their location to load High Fidelity and automatically teleport to them.

### Features ###

* Notifications of connections when they come online.
* Easily go to connections location by the click of a button.

### Getting Started ###

This app has been built with [Electron](https://electron.atom.io/) and can be run with [Node.js](https://nodejs.org/) or run from a packaged version found in the [downloads section](https://bitbucket.org/thecollectivesl/connections-desktop-app/downloads/). Once you have the app running you will need to get an Identity Security Token from the High Fidelity website to be able to access your connections. This can be done by [clicking here](https://highfidelity.com/user/tokens/new) and logging in if you have not done so already. You will need to only tick the identity scope to allow for this app to work correctly.

![security Image](https://bytebucket.org/thecollectivesl/connections-desktop-app/raw/master/img/tokenpage.png)

Once you have this token put it into the settings of your app, hit save and you are good to go.

### Who do I talk to? ###

* [Kurtis Anatine](https://keybase.io/theguywho)